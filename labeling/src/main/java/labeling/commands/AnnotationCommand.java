package labeling.commands;

import labeling.model.AnnotationStorage;

public abstract class AnnotationCommand implements Command {

    private AnnotationStorage annotationStorage;

    public AnnotationCommand(AnnotationStorage annotationStorage) {
        this.annotationStorage = annotationStorage;
    }

    public AnnotationStorage getAnnotationStorage() {
        return annotationStorage;
    }
}
