package labeling.commands;

import java.util.Stack;

public class SimpleCommandStack implements CommandStack {

    private Stack<Command> executedCommands = new Stack<>();
    private Stack<Command> undoneCommands = new Stack<>();
    private final int STACK_SIZE_MAX = 50;

    @Override
    public void addAndExecute(Command command) {
        command.execute();
        executedCommands.push(command);
        undoneCommands.clear();
        if(executedCommands.size() > STACK_SIZE_MAX){
            executedCommands.remove(0);
        }
    }

    @Override
    public void undoLastCommand() {
        if (executedCommands.size() > 0) {
            Command command = executedCommands.pop();
            command.undo();
            undoneCommands.push(command);
        }
    }

    @Override
    public void redoCommand() {
        if (undoneCommands.size() > 0) {
            Command command = undoneCommands.pop();
            command.execute();
            executedCommands.push(command);
        }
    }
}
