package labeling.commands;

import labeling.domain.Annotation;
import labeling.model.AnnotationStorage;

public class AnnotationDeleteCommand extends AnnotationCommand {

    private int index;
    private Annotation annotation;

    public AnnotationDeleteCommand(AnnotationStorage annotationStorage, int index) {
        super(annotationStorage);
        this.index = index;
    }

    @Override
    public void execute() {
        annotation = getAnnotationStorage().getAnnotations().get(index);
        getAnnotationStorage().deleteAnnotation(index);

    }

    @Override
    public void undo() {
        getAnnotationStorage().addAnnotation(index, annotation);
    }
}
