package labeling.commands;

public interface CommandStack {

    void addAndExecute(Command command);
    void undoLastCommand();
    void redoCommand();

}
