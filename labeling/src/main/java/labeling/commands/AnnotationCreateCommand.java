package labeling.commands;

import labeling.model.AnnotationStorage;

public class AnnotationCreateCommand extends AnnotationCommand {

    public AnnotationCreateCommand(AnnotationStorage annotationStorage) {
        super(annotationStorage);
    }

    @Override
    public void execute() {
        getAnnotationStorage().createNewAnnotation();
    }

    @Override
    public void undo() {
        getAnnotationStorage().deleteAnnotation(getAnnotationStorage().getAnnotations().size()-1);
    }
}
