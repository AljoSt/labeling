package labeling.auxiliary.animation;


import labeling.auxiliary.Printer;

public class ValueAnimation {

    private long duration;
    private double startValue;
    private double endValue;
    private AnimationListener animationListener;


    public ValueAnimation(long duration, double startValue, double endValue, AnimationListener animationListener) {
        this.duration = duration;
        this.startValue = startValue;
        this.endValue = endValue;
        this.animationListener = animationListener;
    }

    public void start() {
        final long startTime = System.currentTimeMillis();
        final long endTime = startTime + duration;


        (new Thread(new Runnable() {
            @Override
            public void run() {
                long currTime = startTime;
                double animatedValue = startValue;
                while (currTime < endTime) {
                    double percentagePassed = (double) (currTime - startTime) / (double) (endTime - startTime);
                    percentagePassed = Math.min(1, percentagePassed);
                    animatedValue = (1 - percentagePassed) * startValue + percentagePassed * endValue;
                    animationListener.updated(animatedValue);
                    currTime = System.currentTimeMillis();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                animationListener.updated(endValue);
                animationListener.animationEnded();
            }
        })).start();
    }

    public interface AnimationListener {
        void updated(double value);
        void animationEnded();
    }

}
