package labeling.constants.restrictions;

public class InputSizeRestrictions {

    public static int INPUT_IMAGE_MAX_WIDTH = 3000;
    public static int INPUT_IMAGE_MAX_HEIGHT = 3000;

}
