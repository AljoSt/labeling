package labeling.model;

import labeling.domain.Annotation;

import java.util.ArrayList;
import java.util.List;

public class AnnotationStorage {


    private List<Annotation> annotations = new ArrayList<>();
    private List<OnChangeListener> onChangeListeners = new ArrayList<>();


    public AnnotationStorage() {


        createMockAnnotations();
    }

    private void createMockAnnotations() {
        Annotation annotation;
        for (int i = 0; i < 0; i++) {
            annotation = new Annotation();
            annotation.addString("annotation");
            annotations.add(annotation);
        }
    }


    public List<Annotation> getAnnotations() {
        return annotations;
    }


    public void createNewAnnotation() {
        Annotation annotation = new Annotation();
        annotation.addString("annotation");
        addAnnotation(annotations.size(), annotation);
    }

    public void addAnnotation(int index, Annotation annotation){
        annotations.add(index, annotation);
        for (OnChangeListener onChangeListener : onChangeListeners) {
            onChangeListener.onAnnotationAdded(annotations, index);
        }
    }

    public void deleteAnnotation(int index){
        annotations.remove(index);
        for (OnChangeListener onChangeListener : onChangeListeners) {
            onChangeListener.onAnnotationRemoved(annotations, index);
        }
    }


    public void addOnChangeListener(OnChangeListener onChangeListener) {
        onChangeListeners.add(onChangeListener);
    }

    public interface OnChangeListener {
        void onAnnotationAdded(List<Annotation> annotations, int index);
        void onAnnotationRemoved(List<Annotation> annotations, int index);

    }

}
