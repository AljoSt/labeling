package labeling.domain;

import java.util.ArrayList;

public class Annotation {

    private ArrayList<String> strings = new ArrayList<>();
    private int color;

    public Annotation() {


    }

    public void removeString(int idx){
        strings.remove(idx);
    }

    public void addString(String string){
        strings.add(string);
    }

    public ArrayList<String> getStrings() {
        return strings;
    }
}
