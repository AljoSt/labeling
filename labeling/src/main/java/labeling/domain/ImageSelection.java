package labeling.domain;

import labeling.constants.restrictions.InputSizeRestrictions;


public class ImageSelection {

    private boolean[][] selectionMap;

    public ImageSelection(int width, int height) {

        if (!(0 <= width && width <= InputSizeRestrictions.INPUT_IMAGE_MAX_WIDTH)) {
            String s = "width must be in range of 0 ";
            s += Integer.toString(InputSizeRestrictions.INPUT_IMAGE_MAX_WIDTH);
            throw new IllegalArgumentException(s);
        }

        if (!(0 <= height && height <= InputSizeRestrictions.INPUT_IMAGE_MAX_HEIGHT)) {
            String s = "height must be in range of 0 ";
            s += Integer.toString(InputSizeRestrictions.INPUT_IMAGE_MAX_HEIGHT);
            throw new IllegalArgumentException(s);
        }

        selectionMap = new boolean[height][width];
        resetSelection();
    }

    public void setPosition(int x, int y, boolean val) {
        if (!(0 <= y && y < selectionMap.length)) {
            throw new IllegalArgumentException("y not in range of 0 " + Integer.toString(selectionMap.length - 1));
        }
        if (!(0 <= x && x < selectionMap[0].length)) {
            throw new IllegalArgumentException("x not in range of 0 " + Integer.toString(selectionMap[0].length - 1));
        }

        selectionMap[y][x] = val;
    }

    public void resetSelection() {
        for (int j = 0; j < selectionMap.length; j++) {
            for (int i = 0; i < selectionMap[0].length; i++) {
                selectionMap[j][i] = false;
            }
        }
    }


}
