package labeling.gui.image_annotation_view;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import labeling.auxiliary.Printer;
import labeling.auxiliary.animation.ValueAnimation;

public class ImageAnnotationPresenter {

    //    @FXML
    ImageView imageView;
    @FXML
    ImageView navigationImageView;
    @FXML
    Slider zoomSlider;
    @FXML
    TextField zoomTextField;
    @FXML
    AnchorPane anchorPane;
    @FXML
    StackPane stackPane;
    @FXML
    RadioButton moveRadioBtn;
    @FXML
    RadioButton drawRadioBtn;

    Canvas canvas;



    Image baseImage;

    double zoomfactor = 1;
    Rectangle2D viewportRect;

    boolean mousePressed = false;
    double lastX;
    double lastY;

    double viewportCenterX;
    double viewportCenterY;

    final double ANIMATION_SPEED = 5; // [px/ms]

    boolean uiThreadBusy = false;
    boolean animating = false;

    private final int MOVE_ACTION = 1;
    private final int DRAW_ACTION = 2;
    private int currentAction; // will be initialized in code


    @FXML
    private void initialize() {

        Printer.simplePrint("imageView");


        baseImage = new Image("file:///C:/Users/Aljo/Desktop/medium.jpg");

        imageView = new PixelatedImageView();
        imageView.setImage(baseImage);
        navigationImageView.setImage(baseImage);

        zoomSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (animating) {
                    return;
                }
                double zoom = newValue.doubleValue();
                zoomfactor = (Math.round(zoom * 100)) / 100.0;
                zoomTextField.setText(Integer.toString((int) (zoom * 100)) + "%");


                fitViewportToImageView();
                if (viewportRect.getWidth() > baseImage.getWidth() && viewportRect.getHeight() > baseImage.getHeight()) {
                    moveViewportToCenter(true, true);
                }
            }
        });

        viewportRect = new Rectangle2D(40, 35, 110, 110);
        imageView.setViewport(viewportRect);

        imageView.fitWidthProperty().bind(stackPane.widthProperty());
        imageView.fitHeightProperty().bind(stackPane.heightProperty());

        imageView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mousePressed = true;
                lastX = event.getX();
                lastY = event.getY();
            }
        });

        imageView.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                mousePressed = false;
                lastX = 0;
                lastY = 0;
            }
        });


        imageView.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (mousePressed) {
                    double translationX = event.getX() - lastX;
                    translationX = translationX / zoomfactor;
                    double translationY = event.getY() - lastY;
                    translationY = translationY / zoomfactor;

                    viewportCenterX = viewportRect.getWidth() < baseImage.getWidth() ? viewportCenterX - translationX : baseImage.getWidth() / 2;
                    viewportCenterX = Math.max(0, viewportCenterX);
                    viewportCenterX = Math.min(baseImage.getWidth(), viewportCenterX);

                    viewportCenterY = viewportRect.getHeight() < baseImage.getHeight() ? viewportCenterY - translationY : baseImage.getHeight() / 2;
                    viewportCenterY = Math.max(0, viewportCenterY);
                    viewportCenterY = Math.min(baseImage.getHeight(), viewportCenterY);

                    fitViewportToImageView();

                    lastX = event.getX();
                    lastY = event.getY();
                }
            }
        });

        stackPane.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (viewportRect.getWidth() > baseImage.getWidth() && viewportRect.getHeight() > baseImage.getHeight()) {
                    moveViewportToCenter(true, true);
                }
                if (!animating) {
                    fitViewportToImageView();
                }
            }
        });

        stackPane.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (viewportRect.getWidth() > baseImage.getWidth() && viewportRect.getHeight() > baseImage.getHeight()) {
                    moveViewportToCenter(true, true);
                }
                if (!animating) {
                    fitViewportToImageView();
                }
            }
        });


        stackPane.getChildren().add(imageView);

        canvas = new Canvas(500,500);
        stackPane.getChildren().add(canvas);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(5);
        gc.strokeLine(40, 10, 10, 40);
        gc.drawImage(baseImage, 100,100);


        ToggleGroup toggleGroup = new ToggleGroup();
        moveRadioBtn.setToggleGroup(toggleGroup);
        moveRadioBtn.setUserData(MOVE_ACTION);
        drawRadioBtn.setToggleGroup(toggleGroup);
        drawRadioBtn.setUserData(DRAW_ACTION);
        toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                currentAction = (int) newValue.getUserData();
            }
        });
        currentAction = (int)toggleGroup.getSelectedToggle().getUserData();

    }

    private void moveViewportToCenter(boolean moveX, boolean moveY) {
        animating = true;
        double translationX = moveX ? baseImage.getWidth() / 2 - viewportCenterX : 0;
        double translationY = moveY ? baseImage.getHeight() / 2 - viewportCenterY : 0;

        double distance = Math.sqrt(Math.pow(translationX, 2) + Math.pow(translationY, 2));
        long animationDuration = (long) (distance / ANIMATION_SPEED);

        double startX = viewportCenterX;
        double startY = viewportCenterY;


        new ValueAnimation(animationDuration, 0.0, 1.0, new ValueAnimation.AnimationListener() {
            @Override
            public void updated(double value) {

                if (!uiThreadBusy) {
                    viewportCenterX = startX + value * translationX;
                    viewportCenterY = startY + value * translationY;
                    uiThreadBusy = true;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Printer.simplePrint("UiThread ...");
                            fitViewportToImageView();
                            uiThreadBusy = false;
                        }
                    });
                }
            }

            @Override
            public void animationEnded() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        viewportCenterX = startX + translationX;
                        viewportCenterY = startY + translationY;
                        fitViewportToImageView();
                        animating = false;

                    }
                });
            }
        }).start();

    }

    private void fitViewportToImageView() {
        double aspectRatio = stackPane.getWidth() / stackPane.getHeight();
        double viewportWidth = stackPane.getWidth() / zoomfactor;
        double viewportHeight = viewportWidth / aspectRatio;
        viewportRect = new Rectangle2D(viewportCenterX - viewportWidth / 2, viewportCenterY - viewportHeight / 2, viewportWidth, viewportHeight);
        imageView.setViewport(viewportRect);
    }


}
