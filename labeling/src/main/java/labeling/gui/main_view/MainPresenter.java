package labeling.gui.main_view;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class MainPresenter {



    @FXML
    AnchorPane splitPaneRight;

    @FXML
    AnchorPane splitPaneLeft;

    @FXML
    public void initialize(){

    }

    public AnchorPane getSplitPaneRight() {
        return splitPaneRight;
    }

    public AnchorPane getSplitPaneLeft() {
        return splitPaneLeft;
    }
}
