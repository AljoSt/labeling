package labeling.gui;

import com.airhacks.afterburner.injection.Injector;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import labeling.commands.CommandStack;
import labeling.commands.SimpleCommandStack;
import labeling.constants.Constants;
import labeling.gui.annotation_managemernt_view.AnnotationManagementView;
import labeling.gui.image_annotation_view.ImageAnnotationView;
import labeling.gui.main_view.MainPresenter;
import labeling.model.AnnotationStorage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main extends Application {

    private Stage primaryStage;
    private AnchorPane rootLayout;
    private CommandStack commandStack;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle(Constants.APPLICATION_NAME);

        commandStack = new SimpleCommandStack();
        Map<Object, Object> context = new HashMap<>();

        context.put("annotationStorage", new AnnotationStorage());
        context.put("commandStack", commandStack);

        Injector.setConfigurationSource(context::get);

        initRootLayout();
        initMainView();
        bindGlobalKeyCombinations();
    }

    private void bindGlobalKeyCombinations() {

        final KeyCombination redoCombination = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        final KeyCombination undoCombination = new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN);
        rootLayout.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (undoCombination.match(event)) {
                    commandStack.undoLastCommand();
                } else if (redoCombination.match(event)) {
                    commandStack.redoCommand();
                }
            }
        });
    }

    private void initRootLayout() {

        try {
            rootLayout = FXMLLoader.load(getClass().getResource("rootLayout.fxml"));
            primaryStage.setScene(new Scene(rootLayout, 900, 650));
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }

    private void initMainView() {
        BorderPane mainView = null;

        FXMLLoader mainViewLoader = null;
        try {

            mainViewLoader = new FXMLLoader(getClass().getResource("main_view/main.fxml"));
            mainView = mainViewLoader.load();

            AnnotationManagementView annotationManagementView = new AnnotationManagementView();

            AnchorPane.setTopAnchor(annotationManagementView.getView(), 0.0);
            AnchorPane.setRightAnchor(annotationManagementView.getView(), 0.0);
            AnchorPane.setLeftAnchor(annotationManagementView.getView(), 0.0);
            AnchorPane.setBottomAnchor(annotationManagementView.getView(), 0.0);


            ImageAnnotationView imageAnnotationView = new ImageAnnotationView();
            AnchorPane.setTopAnchor(imageAnnotationView.getView(), 0.0);
            AnchorPane.setRightAnchor(imageAnnotationView.getView(), 0.0);
            AnchorPane.setLeftAnchor(imageAnnotationView.getView(), 0.0);
            AnchorPane.setBottomAnchor(imageAnnotationView.getView(), 0.0);

            MainPresenter mainPresenter = mainViewLoader.getController();

            mainPresenter.getSplitPaneRight().getChildren().add(annotationManagementView.getView());
            mainPresenter.getSplitPaneLeft().getChildren().add(imageAnnotationView.getView());



            rootLayout.getChildren().add(mainView);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }


    }

    public static void main(String[] args) {
        launch(args);
    }
}
