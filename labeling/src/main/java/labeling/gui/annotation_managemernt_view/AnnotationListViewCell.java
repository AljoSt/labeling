package labeling.gui.annotation_managemernt_view;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import labeling.auxiliary.Printer;
import labeling.domain.Annotation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AnnotationListViewCell extends ListCell<AnnotationListViewCell.DAO> {


    @FXML
    private Label label;

    private DAO dao;

    private AnnotationManagementPresenter annotationManagementPresenter;

    Pane expandedView;
    Pane collapsedView;

    private List<OnActionListener> onActionListeners = new ArrayList<>();


    public AnnotationListViewCell(AnnotationManagementPresenter annotationManagementPresenter) {


        this.annotationManagementPresenter = annotationManagementPresenter;
        annotationManagementPresenter.addSelectionListener(new AnnotationManagementPresenter.SelectionListener() {
            @Override
            public void onSelectionChanged(int idx) {

                if (!isEmpty()) {
                    dao.selected = idx == getIndex();
                    updateItem(dao, false);
                }

            }
        });

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("annotationListViewCellCollapsed.fxml"));
        try {
            collapsedView = fxmlLoader.load();
            ((Button)collapsedView.lookup("#deleteBtn")).setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    deleteBtnOnClickListener(event);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        fxmlLoader = new FXMLLoader(getClass().getResource("annotationListViewCellExpanded.fxml"));
        try {
            expandedView = fxmlLoader.load();
            ((Button)expandedView.lookup("#deleteBtn")).setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    deleteBtnOnClickListener(event);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    public void initialize() {
        // Controller stuff
    }

    @Override
    public void updateItem(DAO dao, boolean empty) {
        super.updateItem(dao, empty);


        if (dao == null || empty) {
            setGraphic(null);
        } else {
            this.dao = dao;

            String string = "";
            for (String s : dao.getStrings()) {
                string += s;
            }


            if (dao.selected) {
                label = (Label) expandedView.lookup("#label");
                label.setText(string);
                setGraphic(expandedView);
            } else {
                label = (Label) collapsedView.lookup("#label");
                label.setText(string);
                setGraphic(collapsedView);
            }


        }
    }


    private void deleteBtnOnClickListener(ActionEvent actionEvent) {
        for (OnActionListener onActionListener : onActionListeners) {
            onActionListener.onDeleteButtonPressed(getIndex());
        }

    }

    public void addOnActionListener(OnActionListener onActionListener) {
        onActionListeners.add(onActionListener);
    }


    public interface OnActionListener {
        void onDeleteButtonPressed(int index);
    }


    public static class DAO {
        private List<String> strings = new ArrayList<>();
        private boolean selected = false;
        private Pane view;

        public DAO(List<String> strings, boolean selected) {
            this.strings = strings;
            this.selected = selected;
        }

        public List<String> getStrings() {
            return strings;
        }

        public void setStrings(List<String> strings) {
            this.strings = strings;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

    }

}
