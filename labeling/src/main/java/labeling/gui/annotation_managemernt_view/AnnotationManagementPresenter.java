package labeling.gui.annotation_managemernt_view;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

import labeling.auxiliary.Printer;
import labeling.commands.AnnotationCreateCommand;
import labeling.commands.AnnotationDeleteCommand;
import labeling.commands.CommandStack;
import labeling.constants.Constants;
import labeling.domain.Annotation;
import labeling.model.AnnotationStorage;
import labeling.gui.annotation_managemernt_view.AnnotationListViewCell.DAO;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


public class AnnotationManagementPresenter {


    @FXML
    private ListView<DAO> listView;

    @Inject
    private AnnotationStorage annotationStorage;

    @Inject
    private CommandStack commandStack;

    private ObservableList<DAO> observableList = FXCollections.observableArrayList();
    private List<SelectionListener> selectionListeners = new ArrayList<>();

    private int selectedIdx = Constants.INVALID_IDX;


    @FXML
    private void initialize() {

        annotationStorage.addOnChangeListener(new AnnotationStorage.OnChangeListener() {
            @Override
            public void onAnnotationAdded(List<Annotation> annotations, final int index) {
                boolean selected = false;
                observableList.add(index, annotationToDAO(annotations.get(index), selected));
                setSelection(index, true);

            }

            @Override
            public void onAnnotationRemoved(List<Annotation> annotations, int index) {
                observableList.remove(index);
                if (index < selectedIdx) {
                    setSelection(selectedIdx - 1, false);
                } else if (index == selectedIdx) {
                    if (listView.getItems().size() == 0) {
                        selectedIdx = Constants.INVALID_IDX;
                    } else {
                        setSelection(Math.min(selectedIdx, listView.getItems().size() - 1), false);
                    }
                } else {

                }

            }
        });


        List<Annotation> annotations = annotationStorage.getAnnotations();
        setAnnotations(annotations);
        if (listView.getItems().size() > 0) {
            setSelection(0, false);
        }

        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<DAO>() {
            @Override
            public void changed(ObservableValue<? extends DAO> observable, DAO oldValue, DAO newValue) {
                selectedIdx = listView.getSelectionModel().getSelectedIndex();
                for (SelectionListener selectionListener : selectionListeners) {
                    selectionListener.onSelectionChanged(selectedIdx);
                }

            }
        });
    }

    private void setAnnotations(List<Annotation> annotations) {
        observableList.clear();

        List<DAO> daos = new ArrayList<>();
        for (Annotation annotation : annotations) {
            boolean selected = false;
            daos.add(annotationToDAO(annotation, selected));
        }

        observableList.setAll(daos);

        listView.setItems(observableList);
        listView.setCellFactory(new Callback<ListView<DAO>, ListCell<DAO>>() {
            @Override
            public ListCell<DAO> call(ListView<DAO> param) {

                AnnotationListViewCell annotationListViewCell = new AnnotationListViewCell(AnnotationManagementPresenter.this);
                annotationListViewCell.addOnActionListener(new AnnotationListViewCell.OnActionListener() {
                    @Override
                    public void onDeleteButtonPressed(int index) {
                        deleteAnnotation(index);
                    }
                });
                return annotationListViewCell;
            }
        });
    }

    private void setSelection(int idx, boolean scroll) {
        if (idx < 0 || idx > listView.getItems().size() - 1) {
            String s = "Illegal selection index: " + Integer.toString(idx);
            s += " for list of size " + Integer.toString(listView.getItems().size());
            throw new IllegalArgumentException(s);
        }

        selectedIdx = idx;
        for (DAO dao : observableList) {
            dao.setSelected(false);
        }
        observableList.get(selectedIdx).setSelected(true);
        listView.getSelectionModel().select(selectedIdx);
        if(scroll){
            listView.scrollTo(selectedIdx);
        }


    }

    private DAO annotationToDAO(Annotation annotation, boolean selected) {
        return new DAO(annotation.getStrings(), selected);
    }


    @FXML
    private void addAnnotationBtnOnClickListener(ActionEvent event) {
        commandStack.addAndExecute(new AnnotationCreateCommand(annotationStorage));
    }

    private void deleteAnnotation(int index) {
        commandStack.addAndExecute(new AnnotationDeleteCommand(annotationStorage, index));
    }

    public void addSelectionListener(SelectionListener selectionListener) {
        selectionListeners.add(selectionListener);
    }

    public void removeSelectionListerner(SelectionListener selectionListener) {
        selectionListeners.remove(selectionListener);
    }


    public interface SelectionListener {
        void onSelectionChanged(int idx);
    }


}
